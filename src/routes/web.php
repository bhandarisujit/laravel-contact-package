<?php

Route::group(['namespace' => 'Webwide\Contact\Http\Controllers'],function(){
	
	Route::get('contact','ContactController@index')->name('contact');

	Route::post('contact','ContactController@send');
});

/*
Route::post('contact',function(Request $request){
	return $request->all();
});
*/
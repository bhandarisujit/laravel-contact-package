<?php
namespace Webwide\Contact;

use Illuminate\Support\ServiceProvider;

class ContactServiceProvider extends ServiceProvider{

    public function boot(){
	    /**
		 * Routes
		 */
        $this->loadRoutesFrom(__DIR__ . '/routes/web.php');
        	
		/**
		 * Views
		 */
		$this->loadViewsFrom(__DIR__ . '/views', 'contact');
		/**
		 * migrations
		 */
		$this->loadMigrationsFrom(__DIR__.'/Database/migrations');
		/**
		 * migrations
		 */
		$this->mergeConfigFrom(
			__DIR__.'/config/contact.php', 'contact'
		);

		$this->publishes([
			__DIR__.'/config/contact.php' => config_path('contact.php'),
		]);
     }

     public function register(){
         
    }
}

?>
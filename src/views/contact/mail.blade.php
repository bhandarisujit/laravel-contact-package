@component('mail::message')
# Introduction

Query From {{$name}}
<br>
message:
{{$message}}

@component('mail::button', ['url' => ''])
Button Text
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
